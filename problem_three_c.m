%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Problem Three : Financial Time Series
%         Part C : Dynamic Neural Network
%                  One-Step-Prediction                 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
close all;
figurePosition = [350 160 600 600];

%% step 1: get the data

% %load raw csv data, format it and save it for later use
% data = load('FTSE100.csv');
% %flip data to be in chronological order
% data = flipud(data);
% %older data seems to have the sames column values
% %so, delete older data, i.e take only data with row(>=671)
% data = data(671:length(data),:);
% %save data for later use
% save('FTSE100.mat', 'data');
%
% % load data
% % data meanings: Open,High,Low,Close,Adj_Close, Volume Traded
% % 594 rows of data for 855 days
% % for simplicity, we'll consider that each data is for a day
% load('FTSE100.mat', 'data');

% % plot data
% figure(1);clf;
% plot(data);
% legend('Open', 'High', 'Low', 'Close', 'Adj Close', 'Location','northwest');
% return;

%% step 2: build the neural-net prediction model
   
% we're only interested in close price (column 4)
% data we have are the input (t) and the target (x)
% also, we're interested in only x-days
N = 20;
x = (data(1:N,4));
t = linspace(1,N,N)';

% normalize the data, zero mean, unit standard deviation
if (false)
    f_mean = @(i)(i - mean(i));
    f_std = @(i)(i/std(i));
    t = f_std(f_mean(t));
    x = f_std(f_mean(x));
end

% split the data into 2 sets for test/train
N_train = N-1;
N_test =  1;

% take first part of the data as training
x_target_train = x(1:N_train);
x_target_test = x(N_train+1:N);

% the time we use in training and prediction
t_train = t(1:N_train);
t_test = t(N_train+1:N);

% do the experiment several times to
% know the suitable number for neurons in the hidden layer
% or to know the effect of changing the number of neurons
% in the hidden layer
M = 10;
neural_hiddens = linspace(1,M,M);
neural_delays = linspace(1,M,M);
error_mse_train = zeros(1, M);
error_mse_test = zeros(1, M);

for i=1:1
    
    % train the net once
    %neural_hidden = neural_hiddens(i);
    %neural_delay = neural_delays(i);
    neural_hidden = 4;
    neural_delay = 2;
        
    % nonlinear auto-regressive network
    neural_net = narxnet(1:neural_delay,1:neural_delay,neural_hidden);
    [xs,xi,ai,ts] = preparets(neural_net,num2cell(t_train'),{}, ...
        num2cell(x_target_train'));
    neural_net = train(neural_net,xs,ts,xi,ai);
    
    % perform free-run prediction on the training set
    [x_predict_train,xf,af] = neural_net(xs,xi,ai);
    x_predict_train = cell2mat(x_predict_train)';
    
    % perform free-run prediction using a cloosed-loop
    % this gives the same result as the last element
    % of the array resulted from one-step-ahead
    [neural_net_c,xi_c,ai_c] = closeloop(neural_net,xf,af);
    x_predict_test = neural_net_c(num2cell(t_test'),xi_c,ai_c);
    x_predict_test = cell2mat(x_predict_test)';
    
    % perform one-step-ahead prediction
    % no need for closed loop, because the open-loop
    % is used for one-steep-ahead prediction
    % also, the dealy should be removed
    % neural_net_p = removedelay(neural_net);
    % [xs_p,xi_p,ai_p] = preparets(neural_net_p,num2cell(t_train'),{},num2cell(x_target_train'));
    % x_predict_test_ = neural_net_p(xs_p,xi_p,ai_p);
    % x_predict_test_ = cell2mat(x_predict_test_)';
    % figure(2);clf;
    % plot(t(1+neural_delay:N), x_predict_test_, 'g', 'LineWidth', 3);
    
    % calc error for training
    error = x_target_train(neural_delay+1:N_train) - x_predict_train;
    error_rate_train = abs(error);
    error_mse_train(i) = mse(error_rate_train);
    
    % calc error (for training and testing)
    error = x_target_test - x_predict_test;
    error_rate_test = abs(error);
    error_mse_test(i) = mse(error_rate_test);
    
end

% close NN-Tool after training and prediction
nntraintool('close');

%% step 3: drawing

% plot the time series (original vs. predicted)
fig = figure(1); clf;
set(fig, 'OuterPosition', [260 160 800 600]);
hold on;
set(gca,'xlim',[t(1)-1, t(N)+1]);
plot(t(1:N_train), x_target_train, 'r', 'LineWidth', 1);
plot(t(1+neural_delay:N_train), x_predict_train, 'b', 'LineWidth', 1);
plot(t(1:N_train), x_target_train, 'o', 'MarkerFaceColor','r', 'Color', 'w', 'MarkerSize', 6);
plot(t(1+neural_delay:N_train), x_predict_train, 'o', 'MarkerFaceColor','b', 'Color', 'w', 'MarkerSize', 6);
%plot(t(N:N), x_target_test, 'o', 'MarkerFaceColor','r', 'Color', 'w', 'MarkerSize', 6);
%plot(t(N:N), x_predict_test, 'o', 'MarkerFaceColor','b', 'Color', 'w', 'MarkerSize', 6);
scatter(t(N:N), x_target_test, 50, 'r', 'filled');
scatter(t(N:N), x_predict_test, 50, 'b', 'filled');
xlabel('Time (Day)', 'FontSize', 18);
ylabel('Close Price', 'FontSize', 18);
title('One-step-ahead Prediction', 'FontSize', 18);
plot_legend = legend('Target', 'Prediction', 'Location', 'northwest');
set(plot_legend,'FontSize',14);

% % plot the mse errors for train and test
% fig = figure(1); clf;
% set(fig, 'OuterPosition', figurePosition);
% hold on;
% plot(neural_delays, error_mse_train, 'o', 'MarkerFaceColor','r', 'Color', 'w');
% plot(neural_delays, error_mse_test, 'o', 'MarkerFaceColor','b', 'Color', 'w');
% grid on;
% xlabel('Network Size', 'FontSize', 18);
% ylabel('MSE Error', 'FontSize', 18);
% title('Error Rates vs. Network Delay', 'FontSize', 14);
% plot_legend = legend('Training-set', 'Test-set');
% set(plot_legend,'FontSize',14);








