%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Problem One: Neural Network Approximation
%       Part C: Effect of changing number of hidden neurons
%               on the performance of the network
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
figurePosition = [350 160 600 600];

% get data of distributions
% X is the input and T is the target
N = 1000;
[~, ~, x, t] = problem_one_distribution(N);
% we're only interested in the posterior probability of w1, ie. p(w1|x)
t = t(:,1);

split = 3/4;
N_train = ceil(N*split);
N_test =  N-N_train;

x_train = [x(1:N_train,:); x(N+1:N+N_train,:)];
x_test = [x(N_train+1:N,:); x(N+N_train+1:N+N,:)];
t_train = [t(1:N_train,:); t(N+1:N+N_train,:)];
t_test = [t(N_train+1:N,:); t(N+N_train+1:N+N,:)];

% train neural network using different numbers
% of neurons in hidden layer
M = 20;
neural_hiddens = linspace(1,M,M);
error_mse_train = zeros(1, M);
error_mse_test = zeros(1, M);

for i=1:M

neural_hidden = neural_hiddens(i);
neural_net = feedforwardnet(neural_hidden);
neural_net = train(neural_net, x_train', t_train');

% predict for the train/test sets
p_train = neural_net(x_train')';
p_test = neural_net(x_test')';

% calc error for training-set
error = t_train - p_train;
error_rate_train = abs(error);
error_mse_train(i) = mse(error_rate_train);

% calc error for test-set
error = t_test - p_test;
error_rate_test = abs(error);
error_mse_test(i) = mse(error_rate_test);

end

% close NN-Tool after training and prediction
nntraintool('close');

% plot the mse errors for train and test
fig = figure(1); clf;
set(fig, 'OuterPosition', figurePosition);
hold on;
plot(neural_hiddens, error_mse_train, 'o', 'MarkerFaceColor','r', 'Color', 'w');
plot(neural_hiddens, error_mse_test, 'o', 'MarkerFaceColor','b', 'Color', 'w');
grid on;
xlabel('Network Size', 'FontSize', 18);
ylabel('MSE Error', 'FontSize', 18);
title('Error Rates vs. Network Size', 'FontSize', 14);
plot_legend = legend('Training-set', 'Test-set');
set(plot_legend,'FontSize',14);










