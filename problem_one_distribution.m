% generate data for 2 classes X1, X2 normally
% distributed by mean and covariance m1, m2, c1, c2
function [x1, x2, x, p_target, p_density, boundary_x1_all, boundary_x2_all] = problem_one_distribution(N)

% get data
m1 = [0;4];
m2 = [5;0];
c1 = [2 1; 1 2];
c2 = [1 0; 0 1];
x1 = mvnrnd(m1, c1, N);
x2 = mvnrnd(m2, c2, N);

% fitting the data of the 2 classes to a Gaussian model
x = [x1; x2];
x_model = fitgmdist(x,2);

% proability density function for plotting the contours and 3d data
p_density = @(x,y)pdf(x_model, [x y]);

% calcuate posterior propability, it will be considered
% as the target of the function neural net is trying to approximate
p_target = posterior(x_model ,x);

% now, we want to draw the decision boundary
c1_det = det(c1)^(0.5);
c2_det = det(c2)^(0.5);
lambda = -(0.5)*log(c1_det/c2_det);
c1_inv = c1^-1;
c2_inv = c2^-1;

% calcuate points required to plot the decision boundary (parapola)

% method 1: by direct substitution after getting the eqation
% of the boundary in form x = f(y) (or y = f(x))
y_all = -40:0.1:5.9;
l = size(y_all, 2);
x_all = zeros(1, l);

for i=1:l
    y = y_all(i);
    x_all(i) =  0.01*((-100*y) - ((5.19615)*sqrt(118083 - 20000*y)) + (1900));
end

boundary_x1_all = x_all';
boundary_x2_all = y_all';

% % method 2: difficul solution, by solving the equation
% line_start = -0.4;
% line_stop = 8;
% line_steps = (line_stop - line_start)*100;
% boundary_x1_all = linspace(line_start, line_stop, line_steps)';
% boundary_x2_all = [];
% for i=1:length(boundary_x1_all)
%     disp(i);
%     m = boundary_x1_all(i);
%     syms n;
%     % original equation
%     %eqn = ([m;n]-m1)'*(c1_inv)*([m;n]-m1) == lambda + ([m;n]-m2)'*(c2_inv)*([m;n]-m2);
%     % after reduction
%     eqn = (m*m) + (n*n) - (38*m) + (2*m*n) + (16*n) + 43 + (3*lambda) == 0;
%     n = double(solve(eqn, n));
%     boundary_x2_all = [boundary_x2_all; n'];
% end

end

















