%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Problem Two : Financial Time Series
%       Part A : Static Neural Network
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
close all;
figurePosition = [350 160 600 600];

%% step 1: get the data

% %load raw csv data, format it and save it for later use
% data = load('FTSE100.csv');
% %flip data to be in chronological order
% data = flipud(data);
% %older data seems to have the sames column values
% %so, delete older data, i.e take only data with row(>=671)
% data = data(671:length(data),:);
% %save data for later use
% save('FTSE100.mat', 'data');
% 
% % load data
% % data meanings: Open,High,Low,Close,Adj_Close, Volume Traded
% % 594 rows of data for 855 days
% % for simplicity, we'll consider that each data is for a day
% load('FTSE100.mat', 'data');

% % plot data
% figure(1);clf;
% plot(data);
% legend('Open', 'High', 'Low', 'Close', 'Adj Close', 'Location','northwest');


%% step 2: build the neural-net prediction model

% we're only interested in close price (column 4)
% data we have are the input (t) and the target (x)
x = (data(:,4));
N = length(x);
t = linspace(1,N,N)';

x = flipud(x);

% permute the input input (t) and target (x)
permute = false;
permute_idx = randperm(N);
if (permute)
    x_perm = zeros(N, 1);
    t_perm = zeros(N, 1);
    for i=1:N
        perm_i = permute_idx(i);
        x_perm(i) = x(perm_i);
        t_perm(i) = t(perm_i);
    end
    x = x_perm;
    t = t_perm;
end

% normalize the data, zero mean, unit standard deviation
if (false)
    f_mean = @(i)(i - mean(i));
    f_std = @(i)(i/std(i));
    t = f_std(f_mean(t));
    x = f_std(f_mean(x));
end

% split the data into 2 sets for test/train
split = 5/6;
train_N = ceil(N*split);
test_N =  N-train_N;

% take first part of the data as training
x_target_train = x(1:train_N);
x_target_test = x(train_N+1:N);
net_perform_train = zeros(train_N, 1);
net_perform_test = zeros(test_N, 1);

% the time we use in training and prediction
t_train = t(1:train_N);
t_test = t(train_N+1:N);

% train the net once
neural_hidden = ceil(train_N/200);
neural_hidden = 10;
neural_net = feedforwardnet(neural_hidden, 'trainlm');
neural_net = train(neural_net, t_train', x_target_train');

% perform free-run prediction on the train/test
x_predict_train = neural_net(t_train')';
x_predict_test = neural_net(t_test')';

% calc error (for training and testing)
error = x_target_test - x_predict_test;
error_rate_test = abs(error);

% un-permute the input (t) and target (x)
if (permute)
    x_predict_perm = zeros(N, 1);
    x_predict = [x_predict_train; x_predict_test];
    for i=1:N
        perm_i = permute_idx(i);
        x_perm(i) = x(perm_i);
        t_perm(i) = t(perm_i);
        x_predict_perm(i) = x_predict(perm_i);
    end
    x = x_perm;
    t = t_perm;
    x_predict = x_predict_perm;
    x_predict_train = x_predict(1:train_N);
    x_predict_test = x_predict(train_N+1:N);
end

%% step 3: drawing

if (permute)
    plot_char = '.';
else
    plot_char = '';
end

% % plot error rates (for training and testing) in box plot
% models = ['Neural Network'];
% fig = figure(2); clf;
% set(fig, 'OuterPosition', figurePosition);
% hold on;
% boxplot(error_rate_test);
% grid on;
% title(strcat('Prediction Error'), 'FontSize', 14);
% xlabel(models, 'FontSize', 18);
% ylabel('Error Rates', 'FontSize', 18);

% % plot output x (target vs. predicted)
% fig = figure(4); clf;
% set(fig, 'OuterPosition', figurePosition);
% hold on;
% axisMin = min(min(x_target_test), min(x_predict_test));
% axisMax = max(max(x_target_test), max(x_predict_test));
% axis([axisMin axisMax axisMin axisMax]);
% plot(x_target_test, x_predict_test, '.r');
% plot([axisMin axisMax], [axisMin axisMax], 'b', 'LineWidth', 1);
% daspect([1,1,1]);
% grid on;
% title('LLS Prediction', 'FontSize', 14);
% xlabel('Target', 'FontSize', 18);
% ylabel('Prediction', 'FontSize', 18);

% plot the time series (original vs. predicted)
fig = figure(5); clf;
set(fig, 'OuterPosition', [260 160 800 600]);
subplot(2,1,1);
hold on;
plot(t(train_N+1:N), x(train_N+1:N), strcat('r', plot_char), 'LineWidth', 1);
plot(t(train_N+1:N), x_predict_test, strcat('b', plot_char), 'LineWidth', 1);
xlabel('t', 'FontSize', 18);
ylabel('x(t)', 'FontSize', 18);
title('Prediction for Test-Set', 'FontSize', 14);
legend('Target', 'Prediction', 'Location', 'southwest');
subplot(2,1,2);
hold on;
plot(t, x, strcat('r', plot_char), 'LineWidth', 1);
plot(t(1:train_N), x_predict_train, strcat('b', plot_char), 'LineWidth', 1);
plot(t(train_N+1:N), x_predict_test, strcat('b', plot_char)', 'LineWidth', 3);
xlabel('t', 'FontSize', 18);
ylabel('x(t)', 'FontSize', 18);
title('Prediction for All Data-Set', 'FontSize', 14);
legend('Target', 'Prediction', 'Location', 'northwest');
















