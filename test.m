clc;

% train the network to estimate/predict the posterior
%[x_estimate, net_perform] = neural_train(x, p_target);
%ms_error = abs(x_target - x_estimate);

return;

% box plot for error of prediction
figure (5); clf;
boxplot(ms_error);
% for i=1:2
%     subplot(1,2,i);
%     boxplot(ms_error(:,i));
% end

% plot the posterior problability (actual/predicted) 3d surface
figure(6); clf;
plot_tri = delaunay(x(:,1), x(:,2));
st_title = '';
axis_x_min = min(x(:,1));
axis_x_max = max(x(:,1));
axis_y_min = min(x(:,2));
axis_y_max = max(x(:,2));
axis_z_min = min(min(x_target(:), x_target(:)));
axis_z_max = max(max(x_estimate(:), x_target(:)));
for i=1:2
    if(i == 1)
        st_title = 'P(x1|x2)';
    else
        st_title = 'P(x2|x1)';
    end
    subplot(2,2,i);
    trisurf(plot_tri, x(:,1), x(:,2), x_estimate(:,i), 'EdgeColor', 'none', 'LineStyle', 'none');
    shading interp;
    axis vis3d;
    colormap parula;
    axis([axis_x_min axis_x_max axis_y_min axis_y_max axis_z_min axis_z_max]);
    title([st_title, ' (Predicted)']);
    subplot(2,2,i+2);
    trisurf(plot_tri, x(:,1), x(:,2), x_target(:,i), 'EdgeColor', 'none', 'LineStyle', 'none');
    shading interp;
    axis vis3d;
    colormap summer;
    axis([axis_x_min axis_x_max axis_y_min axis_y_max axis_z_min axis_z_max]);
    title([st_title, ' (Actual)']);
end;


















