%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Problem Two : Chaotic Time Series
%       Part C : ANFIS Prediction Model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
close all;

load mgdata.dat;
time = mgdata(:, 1);
x_t = mgdata(:, 2);

% figure(1);
% plot(time, x_t);
% xlabel('Time (sec)','fontsize',10);
% ylabel('x(t)','fontsize',10);
% title('Mackey-Glass Chaotic Time Series','fontsize',10);

% training data
trn_data = zeros(500, 5);
chk_data = zeros(500, 5);

% prepare training data
trn_data(:, 1) = x_t(101:600);
trn_data(:, 2) = x_t(107:606);
trn_data(:, 3) = x_t(113:612);
trn_data(:, 4) = x_t(119:618);
trn_data(:, 5) = x_t(125:624);

% prepare checking data
chk_data(:, 1) = x_t(601:1100);
chk_data(:, 2) = x_t(607:1106);
chk_data(:, 3) = x_t(613:1112);
chk_data(:, 4) = x_t(619:1118);
chk_data(:, 5) = x_t(625:1124);

% ts starts with t = 0
index = 119:1118;

% figure(2);
% plot(time(index), x_t(index));
% xlabel('Time (sec)','fontsize',10);
% ylabel('x(t)','fontsize',10);
% title('Mackey-Glass Chaotic Time Series','fontsize',10);

% building the ANFIS Model
fismat = genfis1(trn_data);

% the initial MFs for training are shown in the plots
% figure(3);
% for input_index=1:4,
%     subplot(2,2,input_index)
%     [x,y]=plotmf(fismat,'input',input_index);
%     plot(x,y)
%     axis([-inf inf 0 1.2]);
%     xlabel(['Input ' int2str(input_index)],'fontsize',10);
% end

% train the model
[trn_fismat,trn_error, ~, ~, chk_error] = anfis(trn_data, fismat,[],[],chk_data);

% % plot final MF's on x, y, z, u
% figure(4);
% for input_index=1:4,
%     subplot(2,2,input_index)
%     [x,y]=plotmf(trn_fismat,'input',input_index);
%     plot(x,y)
%     axis([-inf inf 0 1.2]);
%     xlabel(['Input ' int2str(input_index)],'fontsize',10);
% end

% % error curves plot
% figure(5);
% epoch_n = 10;
% plot([trn_error chk_error]);
% hold on;
% plot([trn_error chk_error], 'o');
% hold off;
% xlabel('Epochs','fontsize',18);
% ylabel('RMSE (Root Mean Squared Error)','fontsize',18);
% title('Error Curves','fontsize',14);

% plot Target vs. Prediction
figure(6);
input = [trn_data(:, 1:4); chk_data(:, 1:4)];
anfis_output = evalfis(input, trn_fismat);
index = 125:1124;
subplot(2,1,1);
hold on;
plot(time(index), x_t(index), 'r', 'LineWidth', 3);
plot(time(index), anfis_output, 'b', 'LineWidth', 1);
xlabel('t','fontsize',18);
ylabel('x(t)','fontsize',18);
title('MG Series (Target vs.Prediction)','fontsize',16);
plot_legend = legend('Target', 'Prediction', 'Error', 'Location', 'northwest');
set(plot_legend,'FontSize',14);
subplot(2,1,2);
diff = x_t(index)-anfis_output;
plot(time(index), diff, 'b');
xlabel('t','fontsize',18);
ylabel('x(t)','fontsize',18);
title('ANFIS Prediction Errors','fontsize',16);


















