%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Problem Three : Financial Time Series
%         Part B : Dynamic Neural Network
%                  free-run prediction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
close all;
figurePosition = [350 160 600 600];

%% step 1: get the data

% %load raw csv data, format it and save it for later use
% data = load('FTSE100.csv');
% %flip data to be in chronological order
% data = flipud(data);
% %older data seems to have the sames column values
% %so, delete older data, i.e take only data with row(>=671)
% data = data(671:length(data),:);
% %save data for later use
% save('FTSE100.mat', 'data');
%
% % load data
% % data meanings: Open,High,Low,Close,Adj_Close, Volume Traded
% % 594 rows of data for 855 days
% % for simplicity, we'll consider that each data is for a day
% load('FTSE100.mat', 'data');

% % plot data
% figure(1);clf;
% plot(data);
% legend('Open', 'High', 'Low', 'Close', 'Adj Close', 'Location','northwest');
% return;


%% step 2: build the neural-net prediction model

% we're only interested in close price (column 4)
% data we have are the input (t) and the target (x)
x = (data(:,4));
N = length(x);
t = linspace(1,N,N)';

%x = flipud(x);

% normalize the data, zero mean, unit standard deviation
if (true)
    f_mean = @(i)(i - mean(i));
    f_std = @(i)(i/std(i));
    t = f_std(f_mean(t));
    x = f_std(f_mean(x));
end

% split the data into 2 sets for test/train
split = 3/4;
N_train = ceil(N*split);
N_test =  N-N_train;

% take first part of the data as training
x_target_train = x(1:N_train);
x_target_test = x(N_train+1:N);
net_perform_train = zeros(N_train, 1);
net_perform_test = zeros(N_test, 1);

% the time we use in training and prediction
t_train = t(1:N_train);
t_test = t(N_train+1:N);

% train the net once
if (true)
    neural_hidden = ceil(N_train/200);
    neural_hidden = 10;
    neural_delay = 20;
    
    % nonlinear auto-regressive network
    neural_net = narxnet(1:neural_delay,1:neural_delay,neural_hidden);
    [xs,xi,ai,ts] = preparets(neural_net,num2cell(t_train'),{}, num2cell(x_target_train'));
    neural_net = train(neural_net,xs,ts,xi,ai);
end

% perform free-run prediction on the training set
[x_predict_train,xf,af] = neural_net(xs,xi,ai);
x_predict_train = cell2mat(x_predict_train)';

% perform free-run prediction on the test set
% but use a closed-loop neural instead
[neural_net_c,xi,ai] = closeloop(neural_net,xf,af);
x_predict_test = neural_net_c(num2cell(t_test'),xi,ai);
x_predict_test = cell2mat(x_predict_test)';

% calc error (for training and testing)
error = x_target_test - x_predict_test;
error_rate_test = abs(error);

%% step 3: drawing

% % plot error rates (for training and testing) in box plot
% models = ['Neural Network'];
% fig = figure(2); clf;
% set(fig, 'OuterPosition', figurePosition);
% hold on;
% boxplot(error_rate_test);
% grid on;
% title(strcat('Prediction Error'), 'FontSize', 14);
% xlabel(models, 'FontSize', 18);
% ylabel('Error Rates', 'FontSize', 18);

% % plot output x (target vs. predicted)
% fig = figure(4); clf;
% set(fig, 'OuterPosition', figurePosition);
% hold on;
% axisMin = min(min(x_target_test), min(x_predict_test));
% axisMax = max(max(x_target_test), max(x_predict_test));
% axis([axisMin axisMax axisMin axisMax]);
% plot(x_target_test, x_predict_test, '.r');
% plot([axisMin axisMax], [axisMin axisMax], 'b', 'LineWidth', 1);
% daspect([1,1,1]);
% grid on;
% title('LLS Prediction', 'FontSize', 14);
% xlabel('Target', 'FontSize', 18);
% ylabel('Prediction', 'FontSize', 18);

% plot the time series (original vs. predicted)
fig = figure(5); clf;
set(fig, 'OuterPosition', [260 160 800 600]);
subplot(2,1,1);
hold on;
plot(t(N_train+1:N), x(N_train+1:N), 'r', 'LineWidth', 1);
plot(t(N_train+1:N), x_predict_test, 'b', 'LineWidth', 1);
xlabel('Normalized Time', 'FontSize', 18);
ylabel('Normalized Price', 'FontSize', 18);
title('Prediction for Test-Set', 'FontSize', 14);
plot_legend = legend('Target', 'Prediction', 'Location', 'southeast');
set(plot_legend,'FontSize',14);
subplot(2,1,2);
hold on;
plot(t, x, 'r', 'LineWidth', 1);
plot(t(neural_delay+1:N_train), x_predict_train, 'b', 'LineWidth', 1);
plot(t(N_train+1:N), x_predict_test, 'b', 'LineWidth', 3);
xlabel('Normalized Time', 'FontSize', 18);
ylabel('Normalized Price', 'FontSize', 18);
title('Prediction for All Data-Set', 'FontSize', 14);
plot_legend = legend('Target', 'Prediction', 'Location', 'northwest');
set(plot_legend,'FontSize',14);
















