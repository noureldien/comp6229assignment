%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Problem One: Neural Network Approximation
%       Part B: Neural Network Training
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
figurePosition = [350 160 600 600];

% get data of distributions
% X is the input and T is the target
N = 1000;
[~, ~, x, t, ~, boundary_x1, boundary_x2] = problem_one_distribution(N);
% we're only interested in the posterior probability of w1, ie. p(w1|x)
t = t(:,1);

% split the data for training and testing
% training and test partitions are taken from
% both 1st and second half of all the data to enforce humoginity
% for example train:test is 1:1
% then we divide data into 4 quarters (q1, q2, q3, q4)
% where q1, q3 for training and q2, q4 for testing
split = 3/4;
N_train = ceil(N*split);
N_test =  N-N_train;

x_train = [x(1:N_train,:); x(N+1:N+N_train,:)];
x_test = [x(N_train+1:N,:); x(N+N_train+1:N+N,:)];
t_train = [t(1:N_train,:); t(N+1:N+N_train,:)];
t_test = [t(N_train+1:N,:); t(N+N_train+1:N+N,:)];

% train neural network
neural_hidden = 10;
neural_net = feedforwardnet(neural_hidden);
neural_net = train(neural_net, x_train', t_train');
% view(neural_net);

% predict for the train/test sets
p_train = neural_net(x_train')';
p_test = neural_net(x_test')';

% all the predicted data (train + test)
% notice, the predicted data are concatenated
% by the same order the train and test were taken from the original data
p = [p_train(1:N_train); ...
    p_test(1:N_test); ...
    p_train(N_train:2*N_train); ...
    p_test(N_test+1:2*N_test)];

% calc error (for training and testing)
error = t_train - p_train;
error_rate_train = abs(error);
error_mse_train = mse(error);

error = t_test - p_test;
error_rate_test = abs(error);
error_mse_test = mse(error);

% disp(strcat('Hidden neurons: ', int2str(neural_hidden)));
% disp(error_mse_train);
% disp(error_mse_test);
% disp('-------------------');

% plot the predict vs. target (train/test)
% fig = figure(1); clf;
% set(fig, 'OuterPosition', figurePosition);
% subplot(1,2,1);
% hold on;
% axis([-0.1 1.1 -0.1 1.1]);
% %plot(t_train, p_train, 'o', 'MarkerFaceColor','r', 'Color', 'w');
% scatter(t_train, p_train, 'r');
% plot([-0.1 1.1], [-0.1 1.1], 'b', 'LineWidth', 1);
% daspect([1,1,1]);
% grid on;
% xlabel('Target', 'FontSize', 18);
% ylabel('Prediction', 'FontSize', 18);
% title('P(W1|X) Prediction of Training', 'FontSize', 18);
% subplot(1,2,2);
% hold on;
% axis([-0.1 1.1 -0.1 1.1]);
% %plot(t_test, p_test, 'o', 'MarkerFaceColor','r', 'Color', 'w');
% scatter(t_test, p_test, 'r');
% plot([-0.1 1.1], [-0.1 1.1], 'b', 'LineWidth', 1);
% daspect([1,1,1]);
% grid on;
% xlabel('Target', 'FontSize', 18);
% ylabel('Prediction', 'FontSize', 18);
% title('P(W1|X) Prediction of Test', 'FontSize', 18);

% plot error box for predict and train
% fig = figure(2); clf;
% set(fig, 'OuterPosition', figurePosition);
% subplot(1,2,1);
% boxplot(error_rate_train);
% xlabel('Training-set', 'FontSize', 18);
% ylabel('Error Rates', 'FontSize', 18);
% subplot(1,2,2);
% plot_box = boxplot(error_rate_test);
% xlabel('Test-set', 'FontSize', 18);
% ylabel('Error Rates', 'FontSize', 18);

% % plot the posterior problability 2d
% figure(3);clf;
% hold on;
% scatter(x(:,1), x(:,2), 5, t, 'filled');
% scatter(x(:,1), x(:,2), 5, t, 'filled');
% bar = colorbar;
% ylabel(bar, 'Predicted P(W1|X)', 'FontSize', 14);
% plot(boundary_x1, boundary_x2, 'k', 'LineWidth', 2);
% colormap summer;
% axis([-6 8 -4 13]);
% grid on;
% daspect([1,1,1]);
% xlabel('x-axis', 'FontSize', 18);
% ylabel('y-axis', 'FontSize', 18);
% title('Predicted P(W1|X)', 'FontSize', 14);
% plot_legend = legend('Class 1 (W1)', 'Class 2 (W2)', 'Decision Boundary');
% set(plot_legend,'FontSize', 14);






