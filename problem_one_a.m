%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Problem One: Neural Network Approximation
%       Part A: Decision Boundary
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
figurePosition = [350 160 600 600];

% get data for 2 classes representing normal distribution
%[x1, x2, x, p_target, p_density, boundary_x1, boundary_x2] = problem_one_distribution(100);

% % plot the data 2 classes
% fig = figure(1);
% clf;
% set(fig, 'OuterPosition', figurePosition);
% hold on;
% %ezcontour(p_density);
% %plot(x1(:,1),x1(:,2), 'r.');
% %plot(x2(:,1),x2(:,2), 'b.');
% %scatter(x1(:,1),x1(:,2), 10);
% %scatter(x2(:,1),x2(:,2), 10);
% plot(x1(:,1),x1(:,2), 'o', 'MarkerFaceColor','r', 'Color', 'w', 'MarkerSize', 4);
% plot(x2(:,1),x2(:,2), 'o', 'MarkerFaceColor','b', 'Color', 'w', 'MarkerSize', 4);
% plot(boundary_x1, boundary_x2, 'k', 'LineWidth', 2);
% axis([-6 8 -4 13]);
% grid on;
% daspect([1,1,1]);
% xlabel('x-axis', 'FontSize', 18);
% ylabel('y-axis', 'FontSize', 18);
% title('Normal Distribution', 'FontSize', 14);
% plot_legend = legend('Class 1', 'Class 2', 'Decision Boundary');
% set(plot_legend,'FontSize',12);
%return;

% % plot the denisty of the 2 classes on 3d
% % shading interp flat faceted
% plot_color = [237/255 177/255 32/255];
% p_x = [boundary_x1; flipud(boundary_x1)];
% p_y = [boundary_x2; flipud(boundary_x2)];
% p_z = [zeros(460,1);ones(460,1)];
% figure(2);clf;
% ezsurf(p_density);
% colormap summer;
% hold all;
% boundary_x3 = ones(length(boundary_x1), 1);
% plot3(boundary_x1, boundary_x2, 0.002*boundary_x3, 'LineStyle', 'none', ...
%     'Marker', 'o', 'MarkerEdgeColor', plot_color, 'MarkerFaceColor', plot_color, 'MarkerSize', 6);
% plot3(boundary_x1, boundary_x2, 0.088*boundary_x3, 'LineStyle', 'none', ...
%     'Marker', 'o', 'MarkerEdgeColor', plot_color, 'MarkerFaceColor', plot_color, 'MarkerSize', 6);
% plot_fill = fill3(p_x',p_y',p_z','b', 'FaceColor', plot_color);
% alpha(plot_fill, 0.35);
% grid on;
% axis vis3d;
% hold off;
% xlabel('x-axis', 'FontSize', 18);
% ylabel('y-axis', 'FontSize', 18);
% zlabel('z-axis', 'FontSize', 18);
% title('Probability Density of 2 Classes', 'FontSize', 14);
%return;

% % plot the posterior problability 2d
% figure(3);clf;
% scatter(x(:,1), x(:,2), 30, p_target(:,1), 'filled');
% xlabel('x-axis', 'FontSize', 18);
% ylabel('y-axis', 'FontSize', 18);
% title('Posterior Probability of Class 1 - P(W1|X)', 'FontSize', 14);
% bar = colorbar;
% ylabel(bar, 'Posterior Probability of Class 1 - P(W1|X)', 'FontSize', 14);
% colormap summer;
% grid on;
% daspect([1,1,1]);
%return;

% % plot the posterior problability p(x1|x2) 3d surface
% figure(4); clf;
% plot_tri = delaunay(x(:,1), x(:,2));
% trisurf(plot_tri, x(:,1), x(:,2), p_target(:,1));
% %shading interp;
% axis vis3d;
% colormap summer;
% xlabel('x-axis', 'FontSize', 18);
% ylabel('y-axis', 'FontSize', 18);
% zlabel('Probability', 'FontSize', 18);
% title('Posterior Probability of Class 1 - P(W1|X)', 'FontSize', 14);

% % plot the posterior problability 3d missh
% figure(5); clf;
% trimesh(plot_tri, x(:,1), x(:,2), p_target(:,1));

%plot the posterior problability p(x1|x2) 3d surface

% p_up = [];
% x_up = zeros(0,2);
% p_down = [];
% x_down = zeros(0,2);
% for i=1:length(p_target(:,1))
%     if (p_target(i,1) >= 0.3)
%        p_up = [p_up p_target(i,1)];
%        x_up = [x_up; x(i,:)];
%     else
%        p_down = [p_down p_target(i,1)];
%        x_down = [x_down; x(i,:)];
%     end
% end
% figure(5); clf;
% plot_tri = delaunay(x(:,1), x(:,2));
% fig = trisurf(plot_tri, x(:,1), x(:,2), p_target(:,1));
% set(fig, 'FaceColor', 'y');
% hold on;
% plot_tri = delaunay(x_up(:,1), x_up(:,2));
% fig = trisurf(plot_tri, x_up(:,1), x_up(:,2), p_up);
% set(fig, 'FaceColor', 'r');
% hold on;
% plot_tri = delaunay(x_down(:,1), x_down(:,2));
% fig = trisurf(plot_tri, x_down(:,1), x_down(:,2), p_down);
% set(fig, 'FaceColor', 'b');
% hold off;






