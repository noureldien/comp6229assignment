%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Problem Two : Chaotic Time Series
%       Part A : Linear Prediction Model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
figurePosition = [350 160 600 600];

%% step 1: get data from mackey-glass time series

% generate or load the data
[X, T] = problem_two_mg();
N = size(X);
T = T(50:N, 1);
X = X(50:N, 1);
P = 20;

% load mgdata.dat;
% N = length(mgdata);
% T = mgdata(102:N, 1);
% X = mgdata(102:N, 2);

% fig = figure(1); clf;
% set(fig, 'OuterPosition', [260 300 800 400]);
% plot(T, X, 'r');
% set(gca,'xlim',[T(1), T(end)]);
% xlabel('t', 'FontSize', 18);
% ylabel('x(t)', 'FontSize', 18);
% title('Mackey-Glass Time Series', 'FontSize', 14);
% return;

%% step 2: build the linear prediction model

% data we have are the input (t) and the target (x)
% Load auto-mpg data
N = length(X);

% normalize the data, zero mean, unit standard deviation
if (false)
    f_mean = @(i)(i - mean(i));
    f_std = @(i)(i/std(i));
    t = f_std(f_mean(T));
    x = f_std(f_mean(X));
else
    t = T;
    x = X;
end

% split the data into 2 sets
split = 3/4;
N_train = ceil(N*split);
N_test =  N-N_train;

% P is the number of features in the design matrix
P_train = 20;
P_test = 20;

% feature matrix (input) required to get the design matrix (w)
phi_train = zeros(N_train - P_train, P_train);
for i=1:P_train
    phi_train(:,i) = x(1+i:N_train+i-P_train);
end

% the same feature matrix for testing
phi_test = zeros(N_test - P_test, P_test);
for i=1:P_test
    phi_test(:,i) = x(N_train+1+i:N_train+N_test+i-P_train);
end

% take first part of the data as training
x_target_train = x(1:N_train-P_train);
x_target_test = x(N_train+1:N-P_train);

% the time we use in prediction
t_train = t(1:N_train-P_train);
t_test = t(N_train+1:N-P_train);

% least squares regression as pseudo inverse
% w is the design matrix
w = (phi_train'*phi_train\phi_train')*x_target_train;

% predict one-step-ahead for no. of steps equal to test
x_predict_train = phi_train*w;
x_predict_test = phi_test*w;

% calc error (for training and testing)
error = x_target_train - x_predict_train;
error_rate_train = abs(error);

% calc error (for training and testing)
error = x_target_test - x_predict_test;
error_rate_test = abs(error);


%% step 3: drawing

% % plot error rates (for training and testing) in box plot
% fig1 = figure(2); clf;
% set(fig1, 'OuterPosition', figurePosition);
% subplot(1,2,1);
% boxplot(error_rate_train);
% grid on;
% xlabel('Training', 'FontSize', 18);
% ylabel('Error Rates', 'FontSize', 18);
% subplot(1,2,2);
% boxplot(error_rate_test);
% grid on;
% xlabel('Test', 'FontSize', 18);
% ylabel('Error Rates', 'FontSize', 18);

% % plot output x (target vs. predicted)
% fig = figure(4); clf;
% set(fig, 'OuterPosition', figurePosition);
% hold on;
% axisMin = min(min(x_target_test), min(x_predict_test));
% axisMax = max(max(x_target_test), max(x_predict_test));
% axis([axisMin axisMax axisMin axisMax]);
% scatter(x_target_test, x_predict_test, 20, 'r', 'filled');
% plot([axisMin axisMax], [axisMin axisMax], 'b', 'LineWidth', 1);
% daspect([1,1,1]);
% grid on;
% title('LLS Prediction', 'FontSize', 14);
% xlabel('Target', 'FontSize', 18);
% ylabel('Prediction', 'FontSize', 18);

% % plot the time series (original vs. predicted)
% fig = figure(5); clf;
% set(fig, 'OuterPosition', [260 300 800 400]);
% subplot(3,1,1);
% plot(T(N_train+1:N), X(N_train+1:N), 'r');
% set(gca,'xlim',[T(N_train+1), T(N)]);
% xlabel('t', 'FontSize', 18);
% ylabel('x(t)', 'FontSize', 18);
% title('M-G Time Series (Original/Normalized/Predicted)', 'FontSize', 14);
% subplot(3,1,2);
% plot(t(N_train+1:N), x(N_train+1:N), 'r');
% set(gca,'xlim',[t(N_train+1), t(N)]);
% xlabel('t', 'FontSize', 18);
% ylabel('x(t)', 'FontSize', 18);
% subplot(3,1,3);
% plot(t_test, x_predict_test, 'r');
% set(gca,'xlim',[t(N_train+1), t(N)]);
% xlabel('t', 'FontSize', 18);
% ylabel('x(t)', 'FontSize', 18);

% plot the time series (original vs. predicted)
fig = figure(6); clf;
set(fig, 'OuterPosition', [260 300 800 400]);
hold on;
%plot(t, x, 'r', 'LineWidth', 3);
%plot(t_train, x_predict_train, 'b');
plot(t_test, x_target_test, 'r', 'LineWidth', 3);
plot(t_test, x_predict_test, 'b');
set(gca,'xlim',[t(N_train+1), t(N)]);
xlabel('t', 'FontSize', 18);
ylabel('x(t)', 'FontSize', 18);
title('LLS Prediction for Test-set', 'FontSize', 16);
legend('Target', 'Prediction');



