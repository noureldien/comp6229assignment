%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Problem Two : Chaotic Time Series
%       Part B : Neural Net Prediction Model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
close all;
figurePosition = [350 160 600 600];

%% step 1: generate/load data from mackey-glass time series

% either one of the two options
% generate or load data
[X, T] = problem_two_mg();
N = size(X);
T = T(50:N, 1);
X = X(50:N, 1);
N = length(X);

% load mgdata.dat;
% N = length(mgdata);
% T = mgdata(102:N, 1);
% X = mgdata(102:N, 2);

%% step 2: build the neural-net prediction model

% data we have are the input (t) and the target (x)
% Load auto-mpg data
N = length(X);

% normalize the data, zero mean, unit standard deviation
if (true)
    f_mean = @(i)(i - mean(i));
    f_std = @(i)(i/std(i));
    t = f_std(f_mean(T));
    x = f_std(f_mean(X));
else
    t = T;
    x = X;
end

% split the data into 2 sets
split = 3/4;
train_N = ceil(N*split);
test_N =  N-train_N;

% take first part of the data as training
x_target_train = x(1:train_N);
x_target_test = x(train_N+1:N);
x_predict_test = zeros(test_N, 1);
net_perform = zeros(test_N, 1);

% methods of prediction
METHODS = {'free-run', 'one-step-ahead'};
switch(METHODS{1})
    case 'free-run'
        % the time we use training and prediction
        t_train = t(1:train_N,:);
        t_test = t(N_train+1:N);
        
        % train the net
        neural_net = feedforwardnet(20);
        neural_net = train(neural_net, t_train', x_target_train');
        % perform free-run prediction
        x_predict_train = neural_net(t_train')';
        x_predict_test = neural_net(t_test')';
    case 'one-step-ahead'
        % one-step-ahead for no. of steps equal to test
        for i=1:test_N
            disp(strcat('One-step-ahead, i= ', int2str(i)));
            % update the time we use in prediction
            t_train = t(i:train_N+i-1,:);
            t_test = t(train_N+i);
            % train the neural net every time
            neural_net = feedforwardnet(5);
            neural_net = train(neural_net, t_train', x_target_train');
            % predict using neural network
            net_predict = neural_net(t_test);
            net_perform(i) = perform(neural_net, net_predict, x_target_train');
            x_predict_test(i) = net_predict;
            
            % after doing prediction, update the training x
            % by removing the first from the begining and adding
            % the predicted value to the end
            x_target_train = [x_target_train(2:train_N); x_predict_test(i)];
        end
end


% calc error (for training and testing)
error = x_target_test - x_predict_test;
error_rate_test = abs(error);


%% step 3: drawing
% plot error rates (for training and testing) in box plot
models = ['Neural Network'];
fig = figure(2); clf;
set(fig, 'OuterPosition', figurePosition);
hold on;
boxplot(error_rate_test);
grid on;
title(strcat('Prediction Error'), 'FontSize', 14);
xlabel(models, 'FontSize', 18);
ylabel('Error Rates', 'FontSize', 18);

% plot output x (target vs. predicted)
fig = figure(4); clf;
set(fig, 'OuterPosition', figurePosition);
hold on;
axisMin = min(min(x_target_test), min(x_predict_test));
axisMax = max(max(x_target_test), max(x_predict_test));
axis([axisMin axisMax axisMin axisMax]);
plot(x_target_test, x_predict_test, '.r');
plot([axisMin axisMax], [axisMin axisMax], 'b', 'LineWidth', 1);
daspect([1,1,1]);
grid on;
title('LLS Prediction', 'FontSize', 14);
xlabel('Target', 'FontSize', 18);
ylabel('Prediction', 'FontSize', 18);

% plot the time series (original vs. predicted)
fig = figure(5); clf;
set(fig, 'OuterPosition', [260 300 800 400]);
subplot(3,1,1);
plot(T(train_N+1:N), X(train_N+1:N), 'r');
set(gca,'xlim',[T(train_N+1), T(N)]);
xlabel('t', 'FontSize', 18);
ylabel('x(t)', 'FontSize', 18);
title('M-G Time Series (Original/Normalized/Predicted)', 'FontSize', 14);
subplot(3,1,2);
plot(t(train_N+1:N), x(train_N+1:N), 'r');
set(gca,'xlim',[t(train_N+1), t(N)]);
xlabel('t', 'FontSize', 18);
ylabel('x(t)', 'FontSize', 18);
subplot(3,1,3);
plot(t(train_N+1:N), x_predict_test, 'r');
set(gca,'xlim',[t(train_N+1), t(N)]);
xlabel('t', 'FontSize', 18);
ylabel('x(t)', 'FontSize', 18);

% plot the time series (original vs. predicted)
fig = figure(6); clf;
set(fig, 'OuterPosition', [260 300 800 400]);
hold on;
plot(t(train_N+1:N), x(train_N+1:N), 'r');
plot(t(train_N+1:N), x_predict_test, 'b');
set(gca,'xlim',[t(train_N+1), t(N)]);
xlabel('t', 'FontSize', 18);
ylabel('x(t)', 'FontSize', 18);
title('M-G Time Series', 'FontSize', 14);
legend('Target', 'Prediction');



