%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Problem Two : Chaotic Time Series
%       Part B : Neural Net Prediction Model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
close all;
figurePosition = [350 160 600 600];

%% step 1: generate/load data from mackey-glass time series

% generate or load the data
[X, T] = problem_two_mg();
N = size(X);
T = T(50:N, 1);
X = X(50:N, 1);
N = length(X);

%% step 2: build the neural-net prediction model

% data we have are the input (t) and the target (x)
% Load auto-mpg data
N = length(X);

% normalize the data, zero mean, unit standard deviation
if (false)
    f_mean = @(i)(i - mean(i));
    f_std = @(i)(i/std(i));
    t = f_std(f_mean(T));
    x = f_std(f_mean(X));
else
    t = T;
    x = X;
end

% split the data into 2 sets
split = 2/4;
N_train = ceil(N*split);
N_test =  N-N_train;

% take first part of the data as training
x_target_train = x(1:N_train);
x_target_test = x(N_train+1:N);
x_predict_test = zeros(N_test, 1);
net_perform = zeros(N_test, 1);

% the time we use training and prediction
t_train = t(1:N_train);
t_test = t(N_train+1:N);

% train the net
neural_hidden = 10;
neural_delay = 10;

% nonlinear auto-regressive network
neural_net = narxnet(1:neural_delay,1:neural_delay,neural_hidden);
[xs,xi,ai,ts] = preparets(neural_net,num2cell(t_train'),{}, ...
    num2cell(x_target_train'));
neural_net = train(neural_net,xs,ts,xi,ai);

% perform free-run prediction on the training set
[x_predict_train,xf,af] = neural_net(xs,xi,ai);
x_predict_train = cell2mat(x_predict_train)';

% methods of prediction
METHODS = {'free-run', 'one-step-ahead'};
METHOD = METHODS{1};
switch(METHOD)
    case 'free-run'
        % perform free-run prediction using a cloosed-loop
        % this gives the same result as the last element
        % of the array resulted from one-step-ahead
        [neural_net_c,xi_c,ai_c] = closeloop(neural_net,xf,af);
        x_predict_test = neural_net_c(num2cell(t_test'),xi_c,ai_c);
        x_predict_test = cell2mat(x_predict_test)';
        
        % view(neural_net);
        % view(neural_net_c);
        
    case 'one-step-ahead'
        % one-step-ahead for no. of steps equal to test
        % no need for closed loop, because the open-loop
        % is used for one-steep-ahead prediction
        % also, the dealy should be removed
        neural_net_p = removedelay(neural_net);
        [xs_p,xi_p,ai_p] = preparets(neural_net_p,num2cell(t_test'),{},num2cell(x_target_test'));
        x_predict_test = neural_net_p(xs_p,xi_p,ai_p);
        x_predict_test = cell2mat(x_predict_test)';
        
        %perform
        
        %view(neural_net);
        %view(neural_net_p);
end

% close NN-Tool after training and prediction
nntraintool('close');

% calc error (for training and testing)
%error = x_target_test - x_predict_test;
%error_rate_test = abs(error);


%% step 3: drawing
% plot error rates (for training and testing) in box plot
fig = figure(2); clf;
set(fig, 'OuterPosition', figurePosition);
hold on;
boxplot(error_rate_test);
grid on;
title(strcat('Prediction Error'), 'FontSize', 14);
xlabel('Neural Network', 'FontSize', 18);
ylabel('Error Rates', 'FontSize', 18);

% % plot output x (target vs. predicted)
% fig = figure(4); clf;
% set(fig, 'OuterPosition', figurePosition);
% hold on;
% axisMin = min(min(x_target_test), min(x_predict_test));
% axisMax = max(max(x_target_test), max(x_predict_test));
% axis([axisMin axisMax axisMin axisMax]);
% plot(x_target_test, x_predict_test, '.r');
% plot([axisMin axisMax], [axisMin axisMax], 'b', 'LineWidth', 1);
% daspect([1,1,1]);
% grid on;
% title('LLS Prediction', 'FontSize', 14);
% xlabel('Target', 'FontSize', 18);
% ylabel('Prediction', 'FontSize', 18);

% % plot the time series (original vs. predicted)
% fig = figure(5); clf;
% set(fig, 'OuterPosition', [260 300 800 400]);
% subplot(3,1,1);
% plot(T(train_N+1:N), X(train_N+1:N), 'r');
% set(gca,'xlim',[T(train_N+1), T(N)]);
% xlabel('t', 'FontSize', 18);
% ylabel('x(t)', 'FontSize', 18);
% title('M-G Time Series (Original/Normalized/Predicted)', 'FontSize', 14);
% subplot(3,1,2);
% plot(t(train_N+1:N), x(train_N+1:N), 'r');
% set(gca,'xlim',[t(train_N+1), t(N)]);
% xlabel('t', 'FontSize', 18);
% ylabel('x(t)', 'FontSize', 18);
% subplot(3,1,3);
% plot(t(train_N+1:N), x_predict_test, 'r');
% set(gca,'xlim',[t(train_N+1), t(N)]);
% xlabel('t', 'FontSize', 18);
% ylabel('x(t)', 'FontSize', 18);

% plot the time series (original vs. predicted)
fig = figure(1); clf;
set(fig, 'OuterPosition', [260 160 800 600]);
switch(METHOD)
    case 'free-run'
        subplot(2,1,1);
        hold on;
        plot(t, x, 'r', 'LineWidth', 2);
        plot(t(1+neural_delay:N_train), x_predict_train, 'b', 'LineWidth', 1);
        plot(t(N_train+1:N), x_predict_test, 'b', 'LineWidth', 1);
        xlabel('t', 'FontSize', 18);
        ylabel('x(t)', 'FontSize', 18);
        title('Free-run Prediction (Train+Test)', 'FontSize', 18);
        plot_legend = legend('Target', 'Prediction', 'Location', 'northwest');
        set(plot_legend,'FontSize', 14);
        subplot(2,1,2);
        hold on;
        plot(t_test, x_target_test, 'r');
        plot(t_test, x_predict_test, 'b');
        set(gca,'xlim',[t_test(1), t_test(end)]);
        xlabel('t', 'FontSize', 18);
        ylabel('x(t)', 'FontSize', 18);
        title('Free-run Prediction (Test)', 'FontSize', 18);
        plot_legend = legend('Target', 'Prediction');
        set(plot_legend,'FontSize', 14);
        
    case 'one-step-ahead'
        hold on;
        plot(t(1+neural_delay:N_train), x_predict_train, 'r', 'LineWidth', 1);
        plot(t(1+neural_delay:N_train+1), x_predict_test, 'b', 'LineWidth', 1);
        xlabel('t', 'FontSize', 18);
        ylabel('x(t)', 'FontSize', 18);
        title('Free-run Prediction (Train+Test)', 'FontSize', 18);
        plot_legend = legend('Target', 'Prediction', 'Location', 'northwest');
        set(plot_legend,'FontSize', 14);        
end




