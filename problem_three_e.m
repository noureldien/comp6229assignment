%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Problem Three : Chaotic Time Series
%         Part D : Time-Series (Periodic) Prediction
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
close all;

%% steps

%T = simplenar_dataset;
T = num2cell(data(:,4)');
N = size(T,2);

split = 3/4;
train_N = ceil(N*split);
test_N =  N-train_N;

first_half = true;
if (first_half)
T_train = T(1:train_N);
T_test = T(train_N+1:N);
else
T_train = T(test_N+1:N);
T_test = T(1:test_N);
end

neural_delay = 2;
neural_net = narnet(1:neural_delay,10);
[xs,xi,ai,ts] = preparets(neural_net,{},{},T_train);
neural_net = train(neural_net,xs,ts,xi,ai);
y = neural_net(xs,xi);
yc = neural_net(T_test, xi);
yc = yc(neural_delay+1:test_N);

X_plot = linspace(1,N,N);
if (first_half)
X_plot_train = X_plot(1:train_N-neural_delay);
X_plot_test = X_plot(train_N+1+neural_delay:N);
else
X_plot_train = X_plot(test_N+1+neural_delay:N);
X_plot_test = X_plot(1:test_N);
end

figure(1);clf;
hold on;
plot(X_plot, cell2mat(T), 'r', 'LineWidth', 2);
plot(X_plot_train, cell2mat(y), 'k', 'LineWidth', 1);
plot(X_plot_test, cell2mat(yc), 'b', 'LineWidth', 2);
