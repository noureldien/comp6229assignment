%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Problem Three : Financial Time Series
%         Part D : Dynamic Neural Network
%                  Another Input              
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
close all;
figurePosition = [350 160 600 600];

% plot error box for predict with 1 input and 2 inputs
fig = figure(2); clf;
set(fig, 'OuterPosition', figurePosition);
subplot(1,2,1);
boxplot(error_mse_train);
xlabel('Prediction (1 Input)', 'FontSize', 18);
ylabel('Error Rates', 'FontSize', 18);
subplot(1,2,2);
plot_box = boxplot(error_mse_train_);
xlabel('Prediction (2 Inputs)', 'FontSize', 18);
ylabel('Error Rates', 'FontSize', 18);

return;

%% step 1: get the data

% load('FTSE100.mat', 'data');

% % plot data
% figure(1);clf;
% plot(data);
% legend('Open', 'High', 'Low', 'Close', 'Adj Close', 'Location','northwest');
% return;

%% step 2: build the neural-net prediction model
   
% we're  interested in close price and trade volume (column 4, 6)
% data we have are the input (t) and the target (x)
% also, we're interested in only x-days
% take the last 2 days from the button, as they are
% the only days that have trade volume
N = 20;
data_ = flip(data);
x = [data_(1:N,4),data_(1:N,6)];
t = linspace(1,N,N)';

% split the data into 2 sets for test/train
N_train = N-1;
N_test =  1;

% take first part of the data as training
x_target_train = x(1:N_train,:);
x_target_test = x(N_train+1:N,:);

% the time we use in training and prediction
t_train = t(1:N_train);
t_test = t(N_train+1:N);

% do the experiment several times to
% know the suitable number for neurons in the hidden layer
% or to know the effect of changing the number of neurons
% in the hidden layer
M = 10;
neural_hiddens = linspace(1,M,M);
error_mse_train = zeros(1, M);
error_mse_train_ = zeros(1, M);
error_mse_test = zeros(1, M);

for i=1:M
    
    % train the net once
    %neural_hidden = neural_hiddens(i);
    neural_hidden = 4;
        
    % feedforward network
    neural_net = feedforwardnet(neural_hidden);
    neural_net = train(neural_net, t_train', x_target_train');
        
    % predict for the train/test sets
    x_predict_train = neural_net(t_train')';
    x_predict_test = neural_net(t_test')';
        
    % calc error for training
    error = x_target_train - x_predict_train;
    error_rate_train = abs(error);
    error_mse_train(i) = mse(error_rate_train);
    
    % calc error (for training and testing)
    error = x_target_test - x_predict_test;
    error_rate_test = abs(error);
    error_mse_test(i) = mse(error_rate_test);
    
    % nonlinear auto-regressive network
    neural_net = narxnet(1:neural_delay,1:neural_delay,neural_hidden);
    [xs,xi,ai,ts] = preparets(neural_net,num2cell(t_train'),{}, ...
        num2cell(x_target_train(:,1)'));
    neural_net = train(neural_net,xs,ts,xi,ai);
    
    % perform free-run prediction on the training set
    [x_predict_train_,xf,af] = neural_net(xs,xi,ai);
    x_predict_train_ = cell2mat(x_predict_train_)';
    error = x_target_train(1:17,1) - x_predict_train_;
    error_rate_train_ = abs(error);
    error_mse_train_(i) = mse(error_rate_train_);
    
end

% close NN-Tool after training and prediction
nntraintool('close');

%% step 3: drawing

% % draw the given data (Close Price, Trade Volume)
% figure(1); clf;
% subplot(2,1,1);
% hold on;
% plot(x(:,1), 'r');
% scatter(t, x(:,1), 20, 'r', 'filled');
% xlabel('Time (Day)', 'FontSize', 18);
% ylabel('Close Price', 'FontSize', 18);
% title('Close Price in 20 Days', 'FontSize', 18);
% subplot(2,1,2);
% hold on;
% plot(x(:,2), 'b');
% scatter(t, x(:,2), 20, 'b', 'filled');
% xlabel('Time (Day)', 'FontSize', 18);
% ylabel('Trade Volume', 'FontSize', 18);
% title('Trade Volume in 20 Days', 'FontSize', 18);

% % plot the time series (original vs. predicted)
% fig = figure(2); clf;
% set(fig, 'OuterPosition', [260 160 800 600]);
% hold on;
% set(gca,'xlim',[t(1)-1, t(N)+1]);
% plot(t(1:N_train), x_target_train(:,1), 'r', 'LineWidth', 1);
% plot(t(1:N_train), x_predict_train(:,1), 'b', 'LineWidth', 1);
% plot(t(1:N_train), x_target_train(:,1), 'o', 'MarkerFaceColor','r', 'Color', 'w', 'MarkerSize', 6);
% plot(t(1:N_train), x_predict_train(:,1), 'o', 'MarkerFaceColor','b', 'Color', 'w', 'MarkerSize', 6);
% scatter(t(N:N), x_target_test(:,1), 50, 'r', 'filled');
% scatter(t(N:N), x_predict_test(:,1), 50, 'b', 'filled');
% xlabel('Time (Day)', 'FontSize', 18);
% ylabel('Close Price', 'FontSize', 18);
% title('One-step-ahead Prediction', 'FontSize', 18);
% plot_legend = legend('Target', 'Prediction', 'Location', 'northwest');
% set(plot_legend,'FontSize',14);

% % plot the mse errors for train and test
% fig = figure(3); clf;
% set(fig, 'OuterPosition', figurePosition);
% hold on;
% plot(neural_hiddens, error_mse_train, 'o', 'MarkerFaceColor','r', 'Color', 'w');
% plot(neural_hiddens, error_mse_test, 'o', 'MarkerFaceColor','b', 'Color', 'w');
% grid on;
% xlabel('Network Size', 'FontSize', 18);
% ylabel('MSE Error', 'FontSize', 18);
% title('Error Rates vs. Network Size', 'FontSize', 14);
% plot_legend = legend('Training-set', 'Test-set');
% set(plot_legend,'FontSize',14);

% plot error box for predict with 1 input and 2 inputs
fig = figure(2); clf;
set(fig, 'OuterPosition', figurePosition);
subplot(1,2,1);
boxplot(error_mse_train);
xlabel('Training-set', 'FontSize', 18);
ylabel('Error Rates', 'FontSize', 18);
subplot(1,2,2);
plot_box = boxplot(error_mse_train_);
xlabel('Test-set', 'FontSize', 18);
ylabel('Error Rates', 'FontSize', 18);










